# Neovim 

> Yet another `nvim` configuration

## Give it a try

### Installation

```bash
git clone https://gitlab.com/pcoves/nvim.git ~/.config/pcoves
NVIM_APPNAME=pcoves nvim
```

### Cleanup

```bash
rm -rf ~/.config/pcoves ~/local/share/pcoves ~/.local/state/pcoves
```
