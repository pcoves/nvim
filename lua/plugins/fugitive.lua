return {
	"tpope/vim-fugitive",
	dependencies = {
		"tpope/vim-rhubarb",
		"shumphrey/fugitive-gitlab.vim",
	},
	cmd = { "G", "Gw", "GBrowse", "GDelete", "Gread", "GRemove", "GRename" },
	keys = {
		{
			"<Leader>G",
			[[ <cmd>Git<CR> ]],
			"Starts git fugitive",
		},
	},
}
