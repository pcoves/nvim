return {
	"nvim-treesitter/nvim-treesitter",
	build = ":TSUpdate",
	dependencies = {
		{
			"NoahTheDuke/vim-just",
			ft = { "\\cjustfile", "*.just", ".justfile" },
		},
	},
	config = function()
		local configs = require("nvim-treesitter.configs")

		configs.setup({
			auto_install = true,
			ensure_installed = {
				"lua",
				"vim",
				--
				"astro",
				"bash",
				"hcl",
				"html",
				"markdown",
				"markdown_inline",
				-- "norg",
				"python",
				"rust",
				"sql",
				"terraform",
				"typescript",
				"xml",
				"yaml",
			},
			sync_install = false,
			highlight = { enable = true },
		})
	end,
}
