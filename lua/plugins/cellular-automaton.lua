return {
	"Eandrju/cellular-automaton.nvim",
	keys = {
		{
			"<Leader><Esc>",
			[[ <cmd>CellularAutomaton make_it_rain<CR> ]],
			desc = "Make it rain",
		},
	},
}
