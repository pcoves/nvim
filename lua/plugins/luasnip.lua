return {
	"L3MON4D3/LuaSnip",
	config = function()
		local luasnip = require("luasnip")
		require("luasnip.loaders.from_lua").lazy_load()

		luasnip.config.set_config({
			history = true,
			updateevents = "TextChanged,TextChangedI",
			enable_autosnippets = true,
		})

		vim.keymap.set({ "i", "s" }, "<C-l>", function()
			if luasnip.expand_or_jumpable() then
				luasnip.expand_or_jump()
			end
		end, { desc = "Snippet next argument", silent = true })

		vim.keymap.set({ "i", "s" }, "<C-h>", function()
			if luasnip.jumpable(-1) then
				luasnip.jump(-1)
			end
		end, { desc = "Snippet previous argument", silent = true })

		vim.keymap.set("i", "<C-j>", function()
			if luasnip.choice_active() then
				luasnip.change_choice(1)
			end
		end, { desc = "Snippet next choice", silent = true })

		vim.keymap.set("i", "<C-k>", function()
			if luasnip.choice_active() then
				luasnip.change_choice(-1)
			end
		end, { desc = "Snippet previous choice", silent = true })
	end,
}
