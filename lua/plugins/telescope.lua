return {
	"nvim-telescope/telescope.nvim",
	branch = "0.1.x",
	dependencies = {
		"ThePrimeagen/git-worktree.nvim",
		"nvim-lua/plenary.nvim",
		"olimorris/persisted.nvim",
	},
	config = function()
		local telescope = require("telescope")

		telescope.setup({
			pickers = {
				buffers = {
					show_all_buffers = true,
					sort_lastused = true,
					mappings = {
						i = {
							["<c-d>"] = "delete_buffer",
						},
					},
				},
			},
		})

		telescope.load_extension("persisted")
		telescope.load_extension("git_worktree")
	end,
	keys = {
		{
			"<Leader><Leader>",
			function()
				require("telescope.builtin").buffers()
			end,
			desc = "Telescope buffers",
		},

		{
			"<Leader>|",
			function()
				require("telescope.builtin").git_files()
			end,
			desc = "Telescope git files",
		},

		{
			"<Leader>:",
			function()
				require("telescope.builtin").git_branches()
			end,
			desc = "Telescope git branches",
		},

		{
			"<Leader>=",
			function()
				require("telescope.builtin").lsp_references()
			end,
			desc = "Telescope LSP references",
		},
		{
			"<Leader>+",
			function()
				require("telescope.builtin").find_files()
			end,
			desc = "Telescope find files",
		},

		{
			"<Leader>/",
			function()
				require("telescope.builtin").live_grep()
			end,
			desc = "Telescope live grep",
		},

		{
			"<Leader>*",
			function()
				require("telescope.builtin").grep_string()
			end,
			desc = "Telescope grep selection or under cursor",
			mode = { "n", "v" },
		},

		{
			"<Leader>?",
			function()
				require("telescope.builtin").help_tags()
			end,
			desc = "Telescope help tags",
		},

		{
			"<Leader><Tab>",
			function()
				require("telescope.builtin").keymaps()
			end,
			desc = "Telescope help tags",
		},

		{
			"<Leader>S",
			function()
				require("telescope").extensions.persisted.persisted()
			end,
			desc = "Persisted select",
		},

		{
			"<Leader>wt",
			function()
				require("telescope").extensions.git_worktree.git_worktrees()
			end,
			desc = "Switch to or delete a git worktree",
		},

		{
			"<Leader>wT",
			function()
				require("telescope").extensions.git_worktree.create_git_worktree()
			end,
			desc = "Create a git worktree",
		},
	},
}
