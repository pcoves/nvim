return {
	"folke/trouble.nvim",
	dependencies = { "kyazdani42/nvim-web-devicons" },
	opts = {},
	keys = {
		{
			"<Leader>!",
			"<cmd>Trouble diagnostics toggle<cr>",
			desc = "Toggle Trouble window",
			silent = true,
		},
	},
}
