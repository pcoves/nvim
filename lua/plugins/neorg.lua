return {
	"nvim-neorg/neorg",
	ft = "norg",
	dependencies = {
		"hrsh7th/nvim-cmp",
		"nvim-neorg/neorg-telescope",
		"nvim-telescope/telescope.nvim",
	},
	version = "*",
	cmd = "Neorg",
	config = true,
	opts = {
		load = {
			["core.defaults"] = {},
			["core.concealer"] = {
				config = {
					dim_code_blocks = { conceal = false },
					folds = false,
					icon_preset = "diamond",
				},
			},
			["core.completion"] = { config = {
				engine = "nvim-cmp",
				name = "[norg]",
			} },
			["core.esupports.metagen"] = {
				config = {
					author = "Pablo COVES",
					type = "auto",
				},
			},
			["core.qol.todo_items"] = {
				config = {
					order = {
						{ "undone", " " },
						{ "pending", "-" },
						{ "done", "x" },
					},
				},
			},
			["core.dirman"] = {
				config = {
					workspaces = { neorg = "~/.neorg" },
					default_workspace = "neorg",
					index = "index.norg",
					autochdir = false,
				},
			},
			["core.ui.calendar"] = {},
			["core.journal"] = { config = {
				strategy = "flat",
				workspace = "neorg",
			} },
			["core.keybinds"] = {},
			["core.looking-glass"] = {},
			["core.export"] = {},
			["core.integrations.telescope"] = {},
			["core.summary"] = {},

			-- Still not working when it comes to complex paths
			["core.tangle"] = {},
		},
	},
	keys = {
		{
			"<Leader>ni",
			function()
				vim.cmd.Neorg({ args = { "index" } })
			end,
			desc = "Open neorg's default index",
		},
		{
			"<Leader>nq",
			function()
				vim.cmd.Neorg({ args = { "return" } })
			end,
			desc = "Delete all neorg's buffers",
		},

		{
			"<LocalLeader>f",
			function()
				vim.cmd.Telescope({ args = { "neorg", "find_norg_files" } })
			end,
			desc = "Find Norg files through Telescope",
			ft = "norg",
		},
		{
			"<LocalLeader>l",
			function()
				vim.cmd.Telescope({ args = { "neorg", "find_linkable" } })
			end,
			desc = "Find linkable Norg entry through Telescope",
			ft = "norg",
		},
		{
			"<LocalLeader><LocalLeader>l",
			function()
				vim.cmd.Telescope({ args = { "neorg", "insert_link" } })
			end,
			desc = "Insert link to Norg entry through Telescope",
			ft = "norg",
			mode = { "n", "i" },
		},
		{
			"<LocalLeader><LocalLeader>f",
			callback = function()
				vim.cmd.Telescope({ args = { "neorg", "insert_file_link" } })
			end,
			desc = "Find Norg files through Telescope and insert link to it",
			ft = "norg",
			mode = { "n", "i" },
		},

		{
			"gf",
			"<Plug>(neorg.looking-glass.magnify-code-block)",
			ft = "norg",
		},
	},
}
