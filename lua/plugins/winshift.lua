return {
	"sindrets/winshift.nvim",
	cmd = { "WinShift" },
	keys = {
		{ "<Leader>ww", ":WinShift<CR>", desc = "WinShift" },
	},
}
