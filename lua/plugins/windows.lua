return {
	"anuvyklack/windows.nvim",
	dependencies = {
		{ "anuvyklack/middleclass" },
		{ "anuvyklack/animation.nvim" },
	},
	config = function()
		require("windows").setup()

		local vimResized = vim.api.nvim_create_augroup("VimResized", { clear = true })
		vim.api.nvim_create_autocmd("VimResized", {
			group = vimResized,
			pattern = "*",
			command = "wincmd =",
			desc = "Automagically resize Vim's windows when host window gets updated",
		})
	end,
	keys = {
		{ "<C-w>z", ":WindowsMaximize<CR>", desc = "Windows maximize toggle" },
		{ "<C-w>=", ":WindowsEqualize<CR>", desc = "Windows equalize" },
		{ "<C-w>-", ":WindowsMaximizeVertically<CR>", desc = "Windows maximize vertically" },
		{ "<C-w>\\", ":WindowsMaximizeHorizontally<CR>", desc = "Windows maximize horizontally" },
	},
}
