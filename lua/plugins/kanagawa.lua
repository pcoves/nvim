return {
	"rebelot/kanagawa.nvim",
	config = function()
		require("kanagawa").setup({
			dimInactive = true,
			transparent = true,
		})
		vim.cmd("colorscheme kanagawa")
	end,
}
