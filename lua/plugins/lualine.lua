return {
	"nvim-lualine/lualine.nvim",
	dependencies = { "nvim-tree/nvim-web-devicons" },
	opts = {
		sections = {
			lualine_c = {
				function()
					return vim.fn.getcwd()
				end,
				function()
					return vim.fn.expand("%:h")
				end,
				"filename",
			},
		},

		extensions = {
			"fugitive",
			"lazy",
			"mason",
			"quickfix",
		},
	},
}
