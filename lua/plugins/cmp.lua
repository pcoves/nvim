return {
	"hrsh7th/nvim-cmp",
	event = { "InsertEnter", "CmdlineEnter" },
	dependencies = {
		"L3MON4D3/LuaSnip",
		"hrsh7th/cmp-buffer",
		"hrsh7th/cmp-cmdline",
		"hrsh7th/cmp-nvim-lsp",
		"hrsh7th/cmp-path",
		"onsails/lspkind.nvim",
		"hrsh7th/cmp-emoji",
		"saadparwaiz1/cmp_luasnip",
	},
	config = function()
		local cmp = require("cmp")

		cmp.setup({
			snippet = {
				expand = function(args)
					require("luasnip").lsp_expand(args.body)
				end,
			},

			mapping = cmp.mapping.preset.insert({
				["<Up>"] = cmp.mapping.scroll_docs(-4),
				["<Down>"] = cmp.mapping.scroll_docs(4),
				["<C-l>"] = cmp.mapping.confirm({ select = true }),
				["<C-Esc>"] = cmp.mapping.abort(),
			}),

			sources = cmp.config.sources({
				{ name = "luasnip" },
				{ name = "nvim_lsp" },
				{ name = "buffer" },
				{ name = "path" },
				{ name = "emoji" },
				{ name = "neorg" },
			}),

			formatting = {
				format = require("lspkind").cmp_format({
					with_text = true,
					menu = {
						luasnip = "[Snip]",
						nvim_lsp = "[LSP]",
						buffer = "[Buf]",
						path = "[Path]",
						emoji = "[Emoji]",
					},
				}),
			},
		})

		cmp.setup.cmdline({ "/", "?" }, {
			mapping = cmp.mapping.preset.cmdline(),
			sources = { { name = "buffer" } },
		})

		cmp.setup.cmdline(":", {
			mapping = cmp.mapping.preset.cmdline(),
			sources = cmp.config.sources({ { name = "path" } }, { { name = "cmdline" } }),
		})
	end,
}
