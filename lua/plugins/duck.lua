return {
	"tamton-aquib/duck.nvim",
	keys = {
		{
			"<Leader>c",
			function()
				require("duck").hatch("🦀")
			end,
			desc = "Let ferris roam the window",
		},
		{
			"<Leader>C",
			function()
				require("duck").cook()
			end,
			desc = "Capture all that have hatched",
		},
	},
}
