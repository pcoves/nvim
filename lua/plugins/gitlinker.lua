return {
	"ruifm/gitlinker.nvim",
	opts = {},
	keys = {
		{ "<Leader>gy", desc = "Copy link to git{lab,hub} to clipboard", mode = { "n", "v" } },
	},
}
