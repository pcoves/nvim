--        ,gggg,
--       d8" "8I
--       88  ,dP
--    8888888P"
--       88
--       88          ,gggg,gg     ,gggg,  gg     gg
--  ,aa,_88         dP"  "Y8I    d8"  Yb  I8     8I
-- dP" "88P        i8'    ,8I   dP    dP  I8,   ,8I
-- Yb,_,d88b,,_   ,d8,   ,d8b,,dP  ,adP' ,d8b, ,d8I
--  "Y8P"  "Y88888P"Y8888P"`Y88"   ""Y8d8P""Y88P"888
--                                  ,d8I'      ,d8I'
--                                ,dP'8I     ,dP'8I
--                               ,8"  8I    ,8"  8I
--                               I8   8I    I8   8I
--                               `8, ,8I    `8, ,8I
--                                `Y8P"      `Y8P"

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
	local lazyrepo = "https://github.com/folke/lazy.nvim.git"
	local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
	if vim.v.shell_error ~= 0 then
		vim.api.nvim_echo({
			{ "Failed to clone lazy.nvim:\n", "ErrorMsg" },
			{ out, "WarningMsg" },
			{ "\nPress any key to exit..." },
		}, true, {})
		vim.fn.getchar()
		os.exit(1)
	end
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
	spec = { { import = "plugins" } },
	install = { colorscheme = { "kanagawa" } },
	checker = { enabled = true, notify = false },
	change_detection = { notify = false },
})
