vim.keymap.set("n", "j", "gj")
vim.keymap.set("n", "k", "gk")

vim.keymap.set("n", "<Leader>ss", ":set spell!<CR>", { desc = "Toggle spelling", silent = true })
vim.keymap.set("n", "<Leader>sf", ":set spelllang=fr<CR>", { desc = "Set spell lang to French", silent = true })
vim.keymap.set("n", "<Leader>se", ":set spelllang=en<CR>", { desc = "Set spell lang to English", silent = true })

vim.keymap.set("n", "<Leader>b", ":bdelete<CR>", { silent = true })
vim.keymap.set("n", "<Leader>B", ":bdelete!<CR>", { silent = true })

vim.keymap.set("n", "<Leader>q", ":quit<CR>", { silent = true })
vim.keymap.set("n", "<Leader>Q", ":quit!<CR>", { silent = true })

vim.keymap.set("n", "<Leader><Space>", ":let v:hlsearch = !v:hlsearch<CR>", { silent = true })

-- Move visual selection around
-- https://www.youtube.com/watch?v=w7i4amO_zaE
-- https://github.com/ThePrimeagen/init.lua/blob/master/lua/theprimeagen/remap.lua#L5
vim.keymap.set("v", "<Up>", ":m '<-2<CR>gv=gv")
vim.keymap.set("v", "<Down>", ":m '>+1<CR>gv=gv")

vim.keymap.set({ "n", "v" }, "<leader>y", [["+y]])
vim.keymap.set("n", "<leader>Y", [["+Y]])

vim.keymap.set(
	"n",
	"<Leader><",
	":tabnew $MYVIMRC<CR>",
	{ desc = "Open configuration file in a new window", silent = true }
)

vim.keymap.set("c", ";;", function()
	return vim.fn.getcmdtype() == ":" and vim.fn.expand("%:h") .. "/" or ";;"
end, { expr = true })
