vim.g.maplocalleader = " "
vim.g.tex_flavor = "latex"

require("config.opt")
require("config.keymap")
require("config.autocmd")

require("config.lazy")
