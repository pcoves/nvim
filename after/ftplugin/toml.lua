vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.tabstop = 2

vim.api.nvim_create_autocmd({ "BufEnter", "BufWinEnter" }, {
	group = vim.api.nvim_create_augroup("Toml", { clear = true }),
	pattern = "Cargo.toml",
	command = [[ setlocal makeprg=cargo\ build ]],
})
