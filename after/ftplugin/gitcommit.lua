vim.opt_local.shadafile = "NONE"

vim.api.nvim_create_autocmd({ "BufEnter" }, {
	group = vim.api.nvim_create_augroup("Git", { clear = true }),
	pattern = "COMMIT_EDITMSG",
	command = "startinsert",
})
