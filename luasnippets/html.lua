return {
	s(
		"extends",
		fmt('{{% extends "{}.html" %}}{}', {
			i(1, "base"),
			i(0),
		})
	),
	s(
		"import",
		fmt('{{% import "macros/{}.html" as {} %}}{}', {
			i(1, "base"),
			rep(1),
			i(0),
		})
	),
	s(
		"block",
		fmt("{{% block {block} %}}\n" .. "{content}\n" .. "{{% endblock {} %}}", {
			block = i(1, "name"),
			content = i(0, "Lorem ipsum"),
			rep(1),
		})
	),
	s("macro", fmt("{{% macro {}({}) %}}\n" .. "{}\n" .. "{{% endmacro {} %}}", { i(1), i(2), i(0), rep(1) })),
	s(
		"for",
		fmt("{{% for {iterator} in {structure} %}}\n" .. "{content}\n" .. "{{% endfor %}}", {
			iterator = c(1, { i(1, "it"), i(1, "key, values") }),
			structure = i(2, "default(value = [])"),
			content = i(0, "Lorem ipsum"),
		})
	),
}
