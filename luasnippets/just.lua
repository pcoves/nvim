return {
	s(
		"just",
		fmt(
			"set dotenv-load := true\n\n"
				.. "help:\n"
				.. "\t@just --evaluate\n"
				.. "\t@echo ---\n"
				.. "\t@just --list\n\n"
				.. "{}",
			{
				i(0),
			}
		)
	),
}
