local date_en = function()
    return f(function()
        return { os.date("%Y-%m-%d") }
    end)
end
local date_fr = function()
    return f(function()
        return { os.date("%d/%m/%Y") }
    end)
end

local today = function(jump_index)
    jump_index = jump_index or 1
    return c(jump_index, { date_en(), date_fr(), t("\\today") })
end

return {
    s({
        trig = "today",
        name = "Today's date",
        dscr = "Today's date in various formats",
    }, {
        today(1),
    }),
    s({
        trig = "html",
        name = "HTML Modeline",
        dscr = "Add Modeline for HTML filetype",
    }, {
        t("{# vim: set filetype=html: #}"),
    }),
    s({
        trig = "puml",
        name = "Plant UML Modeline",
        dscr = "Add Modeline for Plant UML filetype",
    }, {
        t("{# vim: set filetype=puml: #}"),
    }),
}
